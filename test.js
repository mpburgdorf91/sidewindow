var http = require('http');
var fs = require('fs');
var express = require('express');
var app = express();
var http = require('http').Server(app);

app.use(express.static(__dirname + '/www'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/www/testVideo.html');
  });

app.get('/sidewindow', function (req, res) {
    res.sendFile(__dirname + '/www/sidewindow.html');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});