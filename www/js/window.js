document.addEventListener('DOMContentLoaded', function () {
    init();
});

var video;
var seekVideo;
var seekValue;
var canvas;
var context;
var frame_back;
var frame_back_ctx;
var state = "analyzed";
var w, h, ratio;
var CANVAS_X = 300;
var CANVAS_Y = 300;

var slider_value = $('#slider_value').get(0);
var backward = $('.fa-backward').get(0);
var forward = $('.fa-forward').get(0);
var defaultval = 1.0;
var scalechange = 1;
var moveXAmount = 0;
var moveYAmount = 0;
var isDragging = false;

var clickTime = 0;
var id_counter = 0;
var homeUI_state = 'all';
var lastCurrentTime;
var allEntries = [];
var currentImage;
var currentEntry;
var showUI = false;
var event_state = {};
var durationLog = 0;
var imageOpen = false;
var oldScale = 0;
var oldX = 0;
var oldY = 55;
var diffX = 0;
var diffY = 0;
var canvasX = 0;
var canvasY = 0;
var startX = 0;
var startY = 0;

var MAX_ZOOM = 2.5;
var MIN_ZOOM = 0.2;

function init() {
    video = $('#video').get(0);
    seekVideo = $('#seekVideo').get(0);
    canvas = $('#canvas').get(0);
    canvas_orig = $('#canvas_orig').get(0);
    context = canvas.getContext('2d');
    frame_back = $('#frame_back').get(0);
    frame_back_ctx = frame_back.getContext('2d');
    context_orig = canvas_orig.getContext('2d');
    canvas.width = CANVAS_X;
    canvas.height = CANVAS_Y;
    frame_back.width = CANVAS_X;
    frame_back.height = CANVAS_Y;
    canvas_orig.width = 1200;
    canvas_orig.height = 600;

    frame_back_ctx.fillStyle = "grey";
    frame_back_ctx.fillRect(0, 0, CANVAS_X, CANVAS_Y);

    video.addEventListener("click", mouseHandler);
    $('.ShowHideHandle').on('click', function (e) {
        showUI = !showUI;
        showHideUI();
    });

    $('#move-canvas-handle').on('mousedown touchstart', startMoving);

}

var calculateAspectRatio = function (image) {
    var imageAspectRatio = image.width / image.height;
    var canvasAspectRatio = canvas.width / canvas.height;
    var renderableHeight, renderableWidth, xStart, yStart;
    var AspectRatio = new Object();
    // If image's aspect ratio is less than canvas's we fit on height
    // and place the image centrally along width
    if (imageAspectRatio < canvasAspectRatio) {
        renderableHeight = canvas.height;
        renderableWidth = image.width * (renderableHeight / image.height);
        xStart = (canvas.width - renderableWidth) / 2;
        yStart = 0;
    }

    // If image's aspect ratio is greater than canvas's we fit on width
    // and place the image centrally along height
    else if (imageAspectRatio > canvasAspectRatio) {
        renderableWidth = canvas.width;
        renderableHeight = image.height * (renderableWidth / image.width);
        xStart = 0;
        yStart = (canvas.width - renderableHeight) / 2;
    }

    //keep aspect ratio
    else {
        renderableHeight = canvas.height;
        renderableWidth = canvas.width;
        xStart = 0;
        yStart = 0;
    }
    AspectRatio.renderableHeight = renderableHeight;
    AspectRatio.renderableWidth = renderableWidth;
    AspectRatio.startX = xStart;
    AspectRatio.startY = yStart;
    return AspectRatio;
}

function redraw() {    
    context.clearRect(0, 0, canvas.width, canvas.height);
    // context.scale(defaultval, defaultval);
    // context.drawImage(canvas_orig, diffX, diffY);
    // context.scale(1 / defaultval, 1 / defaultval);
    context.save();
    context.translate(canvas.width/2,canvas.height/2);
    context.scale(defaultval, defaultval);
    context.translate(-600,-300);
    context.drawImage(canvas_orig, diffX, diffY);
    context.restore();
  }

  


// function zoom(event) {
//     defaultval = event.target.value;
//     redraw();
//   }

// function createImage(move) {
//     var ar = calculateAspectRatio(canvas_orig);
//     rw = ar.renderableWidth * defaultval;
//     rh = ar.renderableHeight * defaultval;
//     var w = canvas.width,
//         h = canvas.height,
//         sw = w * defaultval,
//         sh = h * defaultval;
//     context.clearRect(0, 0, canvas.width, canvas.height);
//     if (move) {
//         context.drawImage(canvas_orig, oldX + moveXAmount, oldY + moveYAmount, rw, rh);
//     } else {
//         context.drawImage(canvas_orig, moveXAmount - rw, moveYAmount - rh, rw, rh);
//     }
// }


slider_value.oninput = function (e) {
    // console.log(e.target.value);
    seekValue = e.target.value * 2 / 60;
    // var seekTime = clickTime + ((-1) * Math.sign(seekValue) * 1/60);
    var seekTime = clickTime + seekValue * (-1);
    // console.log("testTime: " + seekTime);
    seekVideo.currentTime = seekTime;
    context.clearRect(0, 0, CANVAS_X, CANVAS_Y);
    context.drawImage(seekVideo, 0, 75, CANVAS_X, 150);
    context_orig.drawImage(seekVideo, 0, 0, 1200, 600);
};

$("#slider_value").on('mousedown touchstart', function (e) {
    seekVideo.currentTime = clickTime + parseFloat(seekValue * (-1));;
    seekVideo.style.left = canvas.offsetLeft + 2 + "px";
    seekVideo.style.top = canvas.offsetTop + 60 + "px";
    // console.log("start seeking");
});

$("#slider_value").on('mouseup touchend', function (e) {
    setTimeout(function () {
        context.drawImage(seekVideo, 0, 56, CANVAS_X, 170);
        context_orig.drawImage(seekVideo, 0, 0, 1200, 600);
        seekVideo.style.top = "-500px";
    }, 400);

    // console.log("end seeking");
    diffX = 0;
    diffY = 0;
    canvasX = 0;
    canvasY = 0;
    startX = 0;
    startY = 0;
});

//Source: https://tympanus.net/codrops/2014/10/30/resizing-cropping-images-canvas/

startMoving = function (e) {
    e.preventDefault();
    e.stopPropagation();
    saveEventState(e);
    $(document).on('mousemove touchmove', moving);
    $(document).on('mouseup touchend', endMoving);
};

endMoving = function (e) {
    e.preventDefault();
    $(document).off('mouseup touchend', endMoving);
    $(document).off('mousemove touchmove', moving);
};

moving = function (e) {
    var mouse = {}, touches;
    e.preventDefault();
    e.stopPropagation();

    touches = e.originalEvent.touches;

    mouse.x = (e.clientX || e.pageX || touches[0].clientX) + $(window).scrollLeft();
    mouse.y = (e.clientY || e.pageY || touches[0].clientY) + $(window).scrollTop();
    $('#move-canvas-handle').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left),
        'top': mouse.y - (event_state.mouse_y - event_state.container_top)
    });
    $('#canvas').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left - 40),
        'top': mouse.y - (event_state.mouse_y - event_state.container_top - 40)
    });

    $('#slider_value').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left) + 77,
        'top': mouse.y - (event_state.mouse_y - event_state.container_top - 28) + 279
    });

    $('.fa-forward').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left) + 315,
        'top': mouse.y - (event_state.mouse_y - event_state.container_top - 28) + 282
    });

    $('.fa-backward').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left) + 54,
        'top': mouse.y - (event_state.mouse_y - event_state.container_top - 28) + 282
    });


    $('#frame_back').offset({
        'left': mouse.x - (event_state.mouse_x - event_state.container_left - 40),
        'top': mouse.y - (event_state.mouse_y - event_state.container_top - 40)
    });

};

saveEventState = function (e) {
    // Save the initial event details and container state
    event_state.container_width = $('#move-canvas-handle').width();
    event_state.container_height = $('#move-canvas-handle').height();
    event_state.container_left = $('#move-canvas-handle').offset().left;
    event_state.container_top = $('#move-canvas-handle').offset().top;
    event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
    event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

    // This is a fix for mobile safari
    // For some reason it does not allow a direct copy of the touches property
    if (typeof e.originalEvent.touches !== 'undefined') {
        event_state.touches = [];
        $.each(e.originalEvent.touches, function (i, ob) {
            event_state.touches[i] = {};
            event_state.touches[i].clientX = 0 + ob.clientX;
            event_state.touches[i].clientY = 0 + ob.clientY;
        });
    }
    event_state.evnt = e;
};

// window.onwheel = function (e) {
//     e.preventDefault();
//     var scale = 0;
//     var posX = 0;
//     var posY = 0;

//     if (imageOpen) {
//         // Your zoom/scale factor
//         scale -= e.deltaY * 0.05;

//         // Your trackpad X and Y positions
//         // posX -= e.deltaX * 2;
//         // posY -= e.deltaY * 2;

//         var newZoom = defaultval + scale;

//         if (newZoom <= MAX_ZOOM && newZoom >= MIN_ZOOM) {
//             defaultval = newZoom;
//         }
//         createImage();
//         console.log("pinch zoom: " + scale);
//     }
// };

window.addEventListener("gesturestart", gestureStart, false);
window.addEventListener("gesturechange", gestureChange, false);
window.addEventListener("gestureend", gestureEnd, false);

function gestureStart(event) {
    event.preventDefault();
}

function gestureChange(event) {
    event.preventDefault();
    // console.log("ZoomeGesture scale:" + event.scale);
    var scale = 0;
    if (event.scale > oldScale) {
        //zoom in -> positive
        scale = event.scale - 1 
    } else {
        //zoom out -> negative        
        scale = event.scale > 1 ? - event.scale * 0.1 : - event.scale;
    }


    if (imageOpen) {
        // Your zoom/scale factor
        scale -= scale * 0.0000000000000001;
        var newZoom = defaultval + scale;

        if (newZoom <= MAX_ZOOM && newZoom >= MIN_ZOOM) {
            defaultval = newZoom;
        }
        redraw();
        // console.log("pinch zoom: " + scale);
        oldScale = event.scale;
    }
}

function gestureEnd(event) {
    event.preventDefault();
}



function snap(x, y, event) {
    diffX = 0;
    diffY = 0;
    canvasX = 0;
    canvasY = 0;
    startX = 0;
    startY = 0;
    defaultval = MIN_ZOOM;
    seekValue = 0;
    $("#slider_value").val(seekValue);
    //$("#slider_value").val(4.0);
    context.clearRect(0, 0, CANVAS_X, CANVAS_Y);
    context.drawImage(video, 0, 55, CANVAS_X, 170);
    var offsetX = event.clientX - CANVAS_X / 2;
    var offsetY = event.clientY - CANVAS_Y / 2;
    canvas.style.left = offsetX + "px";
    canvas.style.top = offsetY + "px";
    frame_back.style.left = offsetX + 4 + "px";
    frame_back.style.top = offsetY + 4 + "px";
    slider_value.style.left = offsetX + 87 + "px";
    slider_value.style.top = offsetY + 273 + "px";
    forward.style.left = offsetX + 277 + "px";
    forward.style.top = offsetY + 270 + "px";
    backward.style.left = offsetX + 11 + "px";
    backward.style.top = offsetY + 270 + "px";
    $("#move-canvas-handle").css("left", offsetX - 40 + "px");
    $("#move-canvas-handle").css("top", offsetY - 40 + "px");
    if (offsetY + 283 > 500) {
        slider_value.style.top = offsetY + 273 - 265 + "px";
        backward.style.top = offsetY + 273 - 265 + "px";
        forward.style.top = offsetY + 273 - 265 + "px";
    }
    ratio = video.videoWidth / video.videoHeight;
    w = video.videoWidth;
    h = parseInt(w / ratio, 10);
    context_orig.drawImage(video, 0, 0, 1200, 600);
    imageOpen = true;
};


function mouseHandler(event) {
    durationLog = new Date();
    var size = getElementCSSSize(this);
    var scaleX = this.videoWidth / size.width;
    var scaleY = this.videoHeight / size.height;

    var rect = this.getBoundingClientRect();  // absolute position of element
    var x = ((event.clientX - rect.left) * scaleX + 0.5) | 0; // round to integer
    var y = ((event.clientY - rect.top) * scaleY + 0.5) | 0;
    // console.log(x, y);
    snap(x, y, event);
    //hide UI?
    $('.container').css("display", "none");
    $('#info').css("display", "none");
    //display analyze and save buton
    $('.buttons_image').css("display", "inline");
    //video.removeEventListener("click", mouseHandler);

    // console.log(video.currentTime);
    clickTime = video.currentTime;
    seekVideo.currentTime = clickTime;
    //set slider to clicktime? or what to do?


    $('#analyze_image').on('click', function (e) {
        durationLog = (new Date() - durationLog) / 1000;
        console.log("Duration: " + durationLog);
        $('.container').css("display", "inline");
        $('#move-canvas-handle').css("top", "-500px");
        $('#canvas').css("top", "-500px");
        $('#frame_back').css("top", "-500px");
        $('#slider_value').css("top", "-500px");
        $('.fa-forward').css("top", "-500px");
        $('.fa-backward').css("top", "-500px");
        $('#analyze_image').off('click');
        $('#save_image').off('click');
        $('#delete_image').off('click');
        $('.buttons_image').css("display", "none");
        currentImage = canvas.toDataURL('image/png');
        currentOrigImage = canvas_orig.toDataURL('image/png');
        selectPOI(clickTime);
        getAnalyzedUi();
        showUI = true;
        showHideUI();
        //video.addEventListener("click", mouseHandler);
    });

    $('#save_image').on('click', function (e) {
        durationLog = (new Date() - durationLog) / 1000;
        console.log("Duration: " + durationLog);
        $('.container').css("display", "inline");
        $('#move-canvas-handle').css("top", "-500px");
        $('#canvas').css("top", "-500px");
        $('#frame_back').css("top", "-500px");
        $('#slider_value').css("top", "-500px");
        $('.fa-forward').css("top", "-500px");
        $('.fa-backward').css("top", "-500px");
        $('#analyze_image').off('click');
        $('#save_image').off('click');
        $('#delete_image').off('click');
        $('.buttons_image').css("display", "none");
        currentImage = canvas.toDataURL('image/png');
        currentOrigImage = canvas_orig.toDataURL('image/png');
        selectPOI(clickTime);
        homeUI_state = 'all';
        getHomeUi();
        showUI = true;
        showHideUI();
        //to-do homeUi -> all
        //video.addEventListener("click", mouseHandler);
    });

    $('#delete_image').on('click', function (e) {
        $('.container').css("display", "inline");
        $('#move-canvas-handle').css("top", "-500px");
        $('#canvas').css("top", "-500px");
        $('#frame_back').css("top", "-500px");
        $('#slider_value').css("top", "-500px");
        $('.fa-forward').css("top", "-500px");
        $('.fa-backward').css("top", "-500px");
        $('#analyze_image').off('click');
        $('#save_image').off('click');
        $('#delete_image').off('click');
        $('.buttons_image').css("display", "none");
        getHomeUi();
        imageOpen = false;
        diffX = 0;
        diffY = 0;
        canvasX = 0;
        canvasY = 0;
        startX = 0;
        startY = 0;
    });

    $("#canvas").mousedown(function (e) {
        isDragging = true;
        // console.log("moveX: " + moveXAmount);
        // console.log("moveY: " + moveYAmount);
        startX = e.clientX;
        startY = e.clientY;

    });

    $(window).mouseup(function () {
        isDragging = false;
        // console.log("moveX: " + moveXAmount);
        // console.log("moveY: " + moveYAmount);
        canvasX = diffX;
        canvasY = diffY;
    });

    $(window).mousemove(function (event) {
        if (isDragging == true) {
            // var cWidth = $("#canvas").width();    
            // moveXAmount = (event.pageX / $(window).width())*cWidth;    
            // moveXAmount = moveXAmount - (cWidth/2);
            // var cHeight = $("#canvas").height(); 
            // moveYAmount = (event.pageY / $(window).height())*cHeight;    
            // moveYAmount = moveYAmount - (cHeight/2);
            moveXAmount = event.pageX - canvas.offsetLeft - 150;
            moveYAmount = event.pageY - canvas.offsetTop - 150;

            diffX = canvasX + (event.clientX - startX) / defaultval;
            diffY = canvasY + (event.clientY - startY) / defaultval;
            //createImage(true);
            redraw();
        }
    });

}

function getElementCSSSize(el) {
    var cs = getComputedStyle(el);
    var w = parseInt(cs.getPropertyValue("width"), 10);
    var h = parseInt(cs.getPropertyValue("height"), 10);
    return { width: w, height: h }
}

var seconds = 0;
var call_time = "00:00";
function incrementSeconds() {
    seconds += 1;
    if (seconds < 10) {
        call_time = "00:0" + seconds;
    } else if (seconds < 60) {
        call_time = "00:" + seconds;
    } else if (seconds >= 60) {
        var minutes = Math.floor(seconds / 60);
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var sec = seconds % 60;
        if (sec < 10) {
            sec = "0" + sec;
        }
        call_time = minutes + ":" + sec;
    }
    $('#call_time').html(call_time);
}


function initAnalyzedUi() {
    $('#call-button').on('click', function (e) {
        changeState("call");
    });

    $('#map-button').on('click', function (e) {
        changeState("map");
    });

    $('.home').on('click', function (e) {
        changeState("home_all");
    });
};

function hideNavigationDialog() {
    $('.navigation-dialog').css("display", "none");
};

//switch not working with strings right now
function changeState(new_state) {
    state = new_state;
    var html = "";
    switch (state) {
        case "analyzed":
            getAnalyzedUi();
            break;

        case "call":
            getCallUi();
            break;

        case "map":
            getMapUi();
            break;

        case "home_all":
            getHomeUi();
            break;
    }
};

function getAnalyzedUi() {
    $("#bounding-box").empty();
    var entry = currentEntry;
    var tmp = new Image();
    tmp.src = entry.img_orig;
    //context_orig.clearRect(0, 0, 1200, 600);
    context_orig.drawImage(tmp, 0, 0, 1200, 600);

    if (entry.poi) {
        $("#bounding-box").load("AnalyzedUi.html", function (data) {
            initAnalyzedUi();
            $('#main-picture .pol-img-top').attr('src', entry.img);
            $('#left-box .pol-title').html(entry.shortTitle);
            $('#left-box .pol-subtitle').html(entry.stars);
            $('.center-box-title').html(entry.title);
            $('.center-box-subtitle').html(entry.stars);
            $('.center-box-data').html(entry.data);
            $('.center-box-data').append(entry.hours);
            $('.center-box-images').html(entry.images);
            setFavourite($('#favourite-button'), entry.favourite);
            $('#favourite-button').on('click', function (e) {
                toggleFavourite(entry.id, $('#favourite-button'));
            });
            $('.pol-img-top').on('click', function (e) {
                viewAndEditImage(entry);
            });
        });
    } else {
        $("#bounding-box").load("EmptyAnalyzedUi.html", function (data) {
            $('.home').on('click', function (e) {
                changeState("home_all");
            });

            $('#main-picture .pol-img-top').attr('src', entry.img);
            $('#call-left-box .pol-title').html(entry.shortTitle);
            $('#call-left-box .pol-subtitle').html(entry.stars);
            $('.center-box-subtitle').html(entry.stars);
            $('#call-center-box').append(entry.map);
            setFavourite($('#favourite-button'), entry.favourite);
            $('#favourite-button').on('click', function (e) {
                toggleFavourite(entry.id, $('#favourite-button'));
            });
            $('.pol-img-top').on('click', function (e) {
                viewAndEditImage(entry);
            });

            $('#start-navigation-button').on('click', function (e) {
                $('.navigation-dialog').css("display", "inline");
                $('.navigation-dialog').html("<p>Navigation for " + entry.shortTitle + " started" +
                    "</br>Distance: 0.5km" +
                    "</br>Estimated Duration: 3 min" +
                    "</p>");
                var test = setTimeout(hideNavigationDialog, 5000);
            });

            $('#add-navigation-button').on('click', function (e) {
                $('.navigation-dialog').css("display", "inline");
                $('.navigation-dialog').html("<p>Waypoint for " + entry.shortTitle + " added" +
                    "</br>Distance: 0.5km" +
                    "</br>Estimated Duration: 3 min" +
                    "</p>");
                var test = setTimeout(hideNavigationDialog, 5000);

            });
        });
    }

};

function getCallUi() {
    $("#bounding-box").empty();
    $("#bounding-box").load("CallUi.html", function (data) {
        var cancel = setInterval(incrementSeconds, 1000);
        $('#end-call-button').on('click', function (e) {
            seconds = 0;
            call_time = "00:00";
            clearInterval(cancel);
            changeState("analyzed");
        });
        var entry = currentEntry;
        $('#main-picture .pol-img-top').attr('src', entry.img);
        $('#call-left-box .pol-title').html(entry.shortTitle);
        $('#call-left-box .pol-subtitle').html(entry.stars);
        $('.center-box-title').html(entry.title);
        $('.center-box-subtitle').html(entry.stars);
        $('.center-box-data').html(entry.data);
        $('.center-box-data').append(entry.hours);
        $('.center-box-images').html(entry.images);
        setFavourite($('#favourite-button'), entry.favourite);
        $('#favourite-button').on('click', function (e) {
            toggleFavourite(entry.id, $('#favourite-button'));
        });
        $('.pol-img-top').on('click', function (e) {
            viewAndEditImage(entry);
        });
    });

};

function getMapUi() {
    $("#bounding-box").empty();
    $("#bounding-box").load("MapUi.html", function (data) {
        $('.home').on('click', function (e) {
            changeState("home_all");
        });

        $('.back').on('click', function (e) {
            changeState("analyzed");
        });
        var entry = currentEntry;
        $('#main-picture .pol-img-top').attr('src', entry.img);
        $('#call-left-box .pol-title').html(entry.shortTitle);
        $('#call-left-box .pol-subtitle').html(entry.stars);
        $('#call-center-box').append(entry.map);
        setFavourite($('#favourite-button'), entry.favourite);
        $('#favourite-button').on('click', function (e) {
            toggleFavourite(entry.id, $('#favourite-button'));
        });

        $('.pol-img-top').on('click', function (e) {
            viewAndEditImage(entry);
        });

        $('#start-navigation-button').on('click', function (e) {
            $('.navigation-dialog').css("display", "inline");
            $('.navigation-dialog').html("<p>Navigation for " + entry.shortTitle + " started" +
                "</br>Distance: 0.5km" +
                "</br>Estimated Duration: 3 min" +
                "</p>");
            var test = setTimeout(hideNavigationDialog, 5000);
        });

        $('#add-navigation-button').on('click', function (e) {
            $('.navigation-dialog').css("display", "inline");
            $('.navigation-dialog').html("<p>Waypoint for " + entry.shortTitle + " added" +
                "</br>Distance: 0.5km" +
                "</br>Estimated Duration: 3 min" +
                "</p>");
            var test = setTimeout(hideNavigationDialog, 5000);

        });
    });
};

function getHomeUi() {
    $("#bounding-box").empty();
    $("#bounding-box").load("HomeUi.html", function (data) {
        //homeUI_state 'all' default
        var entries = allEntries;
        if (homeUI_state == 'pois') {
            entries = filterPOI(entries);
            $("#nav-pois-button").attr('class', 'selected');
        } else if (homeUI_state == 'favourites') {
            entries = filterFavourites(entries);
            $("#nav-favourites-button").attr('class', 'selected');
        } else {
            $("#nav-all-button").attr('class', 'selected');
        }

        var tmpEntries;
        //reorder
        if (entries) {
            tmpEntries = entries.slice();
            tmpEntries.reverse();
        }

        var string = "";
        for (let entry of tmpEntries) {
            var fav;
            if (entry.favourite) {
                fav = "fas fa-heart";
            } else {
                fav = "far fa-heart";
            }
            string += '<div class="polaroid" id="' + entry.id + '">' +
                '<img class="pol-img-top" src="' + entry.img + '" alt="Card image cap">' +
                '<div class="pol-body">' +
                '<div class="pol-text">' +
                '<h5 class="pol-title">' + entry.shortTitle + '</h5>' +
                '<h6 class="pol-subtitle">' + entry.stars + '</h6>' +
                '</div>' +
                '<button id="favourite-button-' + entry.id + '" class="home-fav-button"><i class="' + fav + '"></i></button>' +
                '<button id="share-button"><i class="fas fa-share-square"></i></button>' +
                '</div>' +
                '</div>';
        };
        $("#home-center-box").html(string);



        $(".polaroid").on('click', function (e) {
            var id = e.currentTarget.id;
            var favouriteButton = "favourite-button-" + id;
            var eventButton = (e.target.offsetParent.id == favouriteButton) ? e.target.offsetParent : e.target;
            if (!(e.target.localName == "i" || e.target.localName == "button")) {
                entry = getEntry(id);
                currentEntry = entry;
                getAnalyzedUi();
            } else if (eventButton.id == favouriteButton) {
                toggleFavourite(id, $(eventButton));
            }
        });

        $("#nav-all-button").on('click', function (e) {
            homeUI_state = "all";
            getHomeUi();
        });
        $("#nav-favourites-button").on('click', function (e) {
            homeUI_state = "favourites";
            getHomeUi();
        });
        $("#nav-pois-button").on('click', function (e) {
            homeUI_state = "pois";
            getHomeUi();
        });

    });

};

function toggleFavourite(id, button) {
    entry = allEntries[id];
    entry.favourite = !entry.favourite;
    allEntries[id] = entry;
    setFavourite(button, entry.favourite);
};

function setFavourite(button, favourite) {
    var fav = "";
    if (favourite) {
        fav = "fas fa-heart";
    } else {
        fav = "far fa-heart";
    }
    button.children().attr('class', fav);
}

function getEntry(id) {
    for (let entry of allEntries) {
        if (entry.id == id) {
            return entry;
        }
    }
};

function filterFavourites(entries) {
    var favouriteEntries = [];
    for (let entry of entries) {
        if (entry.favourite) {
            favouriteEntries.push(entry);
        }
    }
    return favouriteEntries;
}

function filterPOI(entries) {
    var poiEntries = [];
    for (let entry of entries) {
        if (entry.poi) {
            poiEntries.push(entry);
        }
    }
    return poiEntries;
}

function selectPOI(currentTime) {
    imageOpen = false;
    diffX = 0;
    diffY = 0;
    canvasX = 0;
    canvasY = 0;
    startX = 0;
    startY = 0;
    if (currentTime == lastCurrentTime) {
        return;
    };

    if (currentTime != seekVideo.currentTime) {
        console.log("timeDiff:" + (currentTime - seekVideo.currentTime));
        currentTime = seekVideo.currentTime;
    }

    //to-do:select depending on time of video
    var entry = {
        id: id_counter,
        clickTime: currentTime,
        seekValue: seekValue,
        img: currentImage,
        img_orig: currentOrigImage,
        zoom: defaultval,
        favourite: false,
        poi: false,
        shortTitle: "IMG " + id_counter,
        title: "IMG_" + id_counter + "_" + currentTime,
        stars: "&#8722 &#8722 &#8722 &#8722 &#8722",
        data: "No data found! Check location on map for info!",
        images: "",
        hours: "",
        map: "<img class='map' src='location_bacchus.jpg'/>"
    };

    if (currentTime >= 24.8 && currentTime <= 32.8) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "Bacchus",
            title: "Weinstube Bacchus",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9733",
            data: "Local cuisine with Mediterranean influence",
            images: "<img class='detail' src='detail1_bacchus.jpg'/><img class='detail' src='detail2_bacchus.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - Sat: 16:00 - 00:00</br>So: closed",
            map: "<img class='map' src='location_bacchus.jpg'/>"
        };
    } else if (currentTime >= 57.0 && currentTime <= 62.5) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "Gerhard",
            title: "Meister Gerhard",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Spanish Tapas Restaurant",
            images: "<img class='detail' src='detail1_gerhard.jpg'/><img class='detail' src='detail2_gerhard.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 16:00 - 00:00",
            map: "<img class='map' src='location_gerhard.jpg'/>"
        };
    } else if (currentTime >= 85.1 && currentTime <= 95.6) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "PHYNE",
            title: "PHYNE Bar",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Cocktailbar, Restaurant, Event Location",
            images: "<img class='detail' src='detail1_phyne.jpg'/><img class='detail' src='detail2_phyne.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 12:00 - 22:00",
            map: "<img class='map' src='location_phyne.jpg'/>"
        };
    } else if (currentTime >= 155 && currentTime <= 164) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "Bhf. Deutz",
            title: "Bahnhof Köln Messe/Deutz",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Öffentlicher Verkehr",
            images: "<img class='detail' src='detail1_deutz.jpg'/><img class='detail' src='detail2_deutz.jpg'/>",
            hours: "<h3>Züge</h3>IC, ICE, RB, RE, S6, S11, S12, S13, S19",
            map: "<img class='map' src='location_deutz.jpg'/>"
        };
    } else if (currentTime >= 183.5 && currentTime <= 192.7) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "Heribert",
            title: "Old St. Heribert & Abbey Deutz",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "orthodox church (1663)",
            images: "<img class='detail' src='detail1_deutz_church.jpg'/><img class='detail' src='detail2_deutz_church.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 16:00 - 19:00",
            map: "<img class='map' src='location_deutz_church.jpg'/>"
        };
    } else if (currentTime >= 198 && currentTime <= 207) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "bonaˊme",
            title: "bonaˊme Köln-Deutz",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Kurdish-Turkish restaurant",
            images: "<img class='detail' src='detail1_boname.jpg'/><img class='detail' src='detail2_boname.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 11:00 - 00:00",
            map: "<img class='map' src='location_boname.jpg'/>"
        };
    } else if (currentTime >= 254.5 && currentTime <= 264.7) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "Cathedral",
            title: "Cologne Cathedral",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Cathedral Church of Saint Peter (1248)",
            images: "<img class='detail' src='detail1_dom.jpg'/><img class='detail' src='detail2_dom.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 6:00 - 21:00",
            map: "<img class='map' src='location_dom.jpg'/>"
        };
    } else if (currentTime >= 273.7 && currentTime <= 301) {

        entry = {
            id: id_counter,
            clickTime: currentTime,
            seekValue: seekValue,
            img: currentImage,
            img_orig: currentOrigImage,
            zoom: defaultval,
            favourite: false,
            poi: true,
            shortTitle: "L'Osteria",
            title: "L'Osteria Köln Gürzenich",
            stars: "&#9733 &#9733 &#9733 &#9733 &#9734",
            data: "Italienisches Restaurant",
            images: "<img class='detail' src='detail1_losteria.jpg'/><img class='detail' src='detail2_losteria.jpg'/>",
            hours: "<h3>Business Hours</h3>Mo - So: 11:00 - 00:00",
            map: "<img class='map' src='location_losteria.jpg'/>"
        };
    }

    id_counter++;
    currentEntry = entry;
    allEntries.push(entry);
    lastCurrentTime = currentTime;
}

function viewAndEditImage(entry) {
    imageOpen = true;
    diffX = 0;
    diffY = 0;
    canvasX = 0;
    canvasY = 0;
    startX = 0;
    startY = 0;
    video.removeEventListener("click", mouseHandler);
    //$("#slider_value").val(entry.zoom);
    $("#slider_value").val(entry.seekValue);
    defaultval = entry.zoom;
    seekValue = entry.seekValue;
    clickTime = entry.clickTime;
    seekVideo.currentTime = clickTime + parseFloat(seekValue * (-1));
    context.clearRect(0, 0, CANVAS_X, CANVAS_Y);
    var tmp = new Image();
    tmp.src = entry.img;
    context.drawImage(tmp, 0, 0);
    canvas.style.left = 500 + "px";
    canvas.style.top = 150 + "px";
    frame_back.style.left = 500 + 4 + "px";
    frame_back.style.top = 150 + 4 + "px";
    slider_value.style.left = 500 + 87 + "px";
    slider_value.style.top = 150 + 274 + "px";
    forward.style.left = 500 + 277 + "px";
    forward.style.top = 150 + 270 + "px";
    backward.style.left = 500 + 11 + "px";
    backward.style.top = 150 + 270 + "px";
    $("#move-canvas-handle").css("left", 500 - 40 + "px");
    $("#move-canvas-handle").css("top", 150 - 40 + "px");
    // tmp.src = entry.img_orig;
    // context_orig.drawImage(tmp, 0, 0, 1200, 600);
    $('.container').css("display", "none");
    //display save button and cancel
    $('.buttons_image').css("display", "inline");
    $('#analyze_image').css("display", "none");
    $('#delete_image').css("display", "none");
    $('#cancel_image').css("display", "inline");

    $('#save_image').on('click', function (e) {
        $('.container').css("display", "inline");
        $('#move-canvas-handle').css("top", "-500px");
        $('#canvas').css("top", "-500px");
        $('#frame_back').css("top", "-500px");
        $('#slider_value').css("top", "-500px");
        $('.fa-forward').css("top", "-500px");
        $('.fa-backward').css("top", "-500px");
        $('#save_image').off('click');
        $('#cancel_image').off('click');
        $('#analyze_image').css("display", "inline");
        $('#delete_image').css("display", "inline");
        $('#cancel_image').css("display", "none");
        $('.buttons_image').css("display", "none");
        currentImage = canvas.toDataURL('image/png');
        entry.img = currentImage;
        entry.zoom = defaultval;
        entry.seekValue = seekValue;
        allEntries[entry.id] = entry;
        $('#main-picture .pol-img-top').attr('src', entry.img);
        video.addEventListener("click", mouseHandler);
        imageOpen = false;
    });

    $('#cancel_image').on('click', function (e) {
        $('.container').css("display", "inline");
        $('#move-canvas-handle').css("top", "-500px");
        $('#canvas').css("top", "-500px");
        $('#frame_back').css("top", "-500px");
        $('#slider_value').css("top", "-500px");
        $('.fa-forward').css("top", "-500px");
        $('.fa-backward').css("top", "-500px");
        $('#save_image').off('click');
        $('#cancel_image').off('click');
        $('#analyze_image').css("display", "inline");
        $('#delete_image').css("display", "inline");
        $('#cancel_image').css("display", "none");
        $('.buttons_image').css("display", "none");
        video.addEventListener("click", mouseHandler);
        imageOpen = false;
    });
}

function showHideUI() {
    if (showUI) {
        $('#bounding-box').css("display", "");
        $('.ShowHideHandle').removeClass("hideUI");
        $('.ShowHideHandle i').removeClass("fas fa-chevron-up");
        $('.ShowHideHandle i').addClass("fas fa-chevron-down");
    } else {
        $('#bounding-box').css("display", "none");
        $('.ShowHideHandle').addClass("hideUI");
        $('.ShowHideHandle i').removeClass("fas fa-chevron-down");
        $('.ShowHideHandle i').addClass("fas fa-chevron-up");
    }
}